#include "SHInteractable.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"

#include "SHCharacter.h"

// Sets default values
ASHInteractable::ASHInteractable()
{
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollider0"));
	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &ASHInteractable::OnBeginOverlap);
	SphereComp->OnComponentEndOverlap.AddDynamic(this, &ASHInteractable::OnEndOverlap);
	RootComponent = SphereComp;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh0"));
	MeshComp->SetupAttachment(RootComponent);
}

void ASHInteractable::Interact(ASHCharacter* CharacterInstigator)
{
	Execute_PlayInteractEffects(this);

	CharacterInstigator->Inventory.Add(PickableItem);
	Destroy();
}

void ASHInteractable::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UKismetSystemLibrary::PrintString(GetWorld(), TEXT("ASHInteractable::OnBeginOverlap"));

	ASHCharacter* Character = Cast<ASHCharacter>(OtherActor);
	if (Character)
	{
		Character->AddInteractable(this);
	}
}

void ASHInteractable::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UKismetSystemLibrary::PrintString(GetWorld(), TEXT("ASHInteractable::OnEndOverlap"));

	ASHCharacter* Character = Cast<ASHCharacter>(OtherActor);
	if (Character)
	{
		Character->DeleteInteractable(this);
	}
}