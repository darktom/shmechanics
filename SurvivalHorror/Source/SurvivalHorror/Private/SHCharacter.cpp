#include "SHCharacter.h"

#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/InputSettings.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "SurvivalHorrorProjectile.h"
#include "Components/SHHealthComponent.h"
#include "SHWeapon.h"
#include "SHInteractable.h"

ASHCharacter::ASHCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComp = CreateDefaultSubobject<USHHealthComponent>(TEXT("HealthComp"));

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true; // Activate the posibility to crouch

	// Create a CameraComponent	
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComp->SetupAttachment(GetCapsuleComponent());
	CameraComp->bUsePawnControlRotation = true;

	// Configure ACharater Default SkeletalMesh
	GetMesh()->SetOnlyOwnerSee(true);
	GetMesh()->bCastDynamicShadow = false;
	GetMesh()->CastShadow = false;
	GetMesh()->SetupAttachment(CameraComp);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	ZoomedFOV = 65.0f;
	ZoomInterpSpeed = 20.0f;

	WeaponSocketName = "GripPoint";

	bDied = false;
	bIsSprinting = false;

	WalkingSpeedMax = 600.0f;
	SprintingSpeedMax = 800.0f;
}

void ASHCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	DefaultFOV = CameraComp->FieldOfView;

	HealthComp->OnHealthChanged.AddDynamic(this, &ASHCharacter::OnHealthChanged);

	// Spawn the default weapon
	/*

	*/
}

// Called every frame
void ASHCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float TargetFOV = bWantsToZoom ? ZoomedFOV : DefaultFOV;
	float NewFOV = FMath::FInterpTo(CameraComp->FieldOfView, TargetFOV, DeltaTime, ZoomInterpSpeed);
	CameraComp->SetFieldOfView(NewFOV);
}

void ASHCharacter::AddInteractable(ASHInteractable* Interactable)
{
	if (Interactable)
	{
		InteractableActors.Add(Interactable);
	}
}

void ASHCharacter::DeleteInteractable(ASHInteractable* Interactable)
{
	if (Interactable)
	{
		InteractableActors.Remove(Interactable);
	}
}

void ASHCharacter::Interact()
{
	if (InteractableActors.Num() > 0)
	{
		if (InteractableActors[0])
		{
			ASHInteractable* Interactable = InteractableActors[0];
			DeleteInteractable(Interactable);
			Interactable->Interact(this);
		}
	}
}

// Input
void ASHCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASHCharacter::StartFireWeapon);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASHCharacter::StopFireWeapon);

	// Bind crouch events
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASHCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ASHCharacter::EndCrouch);

	// Bind zoom events
	PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &ASHCharacter::BeginZoom);
	PlayerInputComponent->BindAction("Zoom", IE_Released, this, &ASHCharacter::EndZoom);

	// Bind sprint events
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ASHCharacter::BeginSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ASHCharacter::EndSprint);

	// Bind interact event
	PlayerInputComponent->BindAction("Interact", IE_Released, this, &ASHCharacter::Interact);
	PlayerInputComponent->BindAction("Reload", IE_Released, this, &ASHCharacter::ReloadWeapon);


	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ASHCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASHCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASHCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASHCharacter::LookUpAtRate);
}

void ASHCharacter::ReloadWeapon()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartReloading();
	}
}

void ASHCharacter::StartFireWeapon()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}


void ASHCharacter::StopFireWeapon()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

void ASHCharacter::BeginCrouch()
{
	Crouch();
}

void ASHCharacter::EndCrouch()
{
	UnCrouch();
}

void ASHCharacter::BeginZoom()
{
	bWantsToZoom = true;
}

void ASHCharacter::EndZoom()
{
	bWantsToZoom = false;
}

void ASHCharacter::BeginSprint()
{
	SprintingSpeedMax = 1000.0f;
	bIsSprinting = true;
	Cast<UCharacterMovementComponent>(GetMovementComponent())->MaxWalkSpeed = SprintingSpeedMax;

	PlaySprintingEffects();
}

void ASHCharacter::EndSprint()
{
	WalkingSpeedMax = 600.0f;

	bIsSprinting = false;
	Cast<UCharacterMovementComponent>(GetMovementComponent())->MaxWalkSpeed = WalkingSpeedMax;

	StopSprintingEffects();
}

void ASHCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ASHCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ASHCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASHCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASHCharacter::OnHealthChanged(USHHealthComponent* HealthComponent, float Health, float HealthDelta, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0.0f && !bDied)
	{
		// Die!
		bDied = true;

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DetachFromControllerPendingDestroy();

		// Enough time to play animation and create an end widget
		SetLifeSpan(10.0f); // After the time will be destroyed

		PlayOnDieEffects();
		OnDieDelegate.Broadcast();
	}
}