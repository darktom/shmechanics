#include "SHWeapon.h"

#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"

#include "DrawDebugHelpers.h" // Debugging lines

#include "SurvivalHorror.h" // Custom surfaces and collisionchannels

// Sets default values
ASHWeapon::ASHWeapon()
{
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;

	AmountDamage = 20.0f;
	RateOfFire = 600.0f;
	BulletsSpread = 2.0f;

	bIsReloading = false;
	MagazineSize = 10;
	CurrentBullets = 4;
	MuzzleSocketName = "Muzzle"; // Default name of socket in the StarterContent FP_GUN
	TracerTargetName = "BeamEnd";
}

// Called when the game starts or when spawned
void ASHWeapon::BeginPlay()
{
	Super::BeginPlay();

	TimeBetweenShots = 60 / RateOfFire;
}

void ASHWeapon::StartFire()
{
	if (CurrentBullets <= 0)
	{
		StartReloading();

	}
	else
	{
		float FirstDelay = LastFireTime + TimeBetweenShots - GetWorld()->TimeSeconds;
		//FirstDelay = FMath::Clamp(FirstDelay, 0.0f, FirstDelay);
		FirstDelay = FMath::Max(FirstDelay, 0.0f);

		GetWorldTimerManager().SetTimer(TimerHandle_TimeBetweenShots, this, &ASHWeapon::Fire, TimeBetweenShots, true, FirstDelay);
	}
}

void ASHWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_TimeBetweenShots);
}

void ASHWeapon::StartReloading()
{
	if (!bIsReloading && CurrentBullets < MagazineSize)
	{
		bIsReloading = true;
		PlayReloadingEffects();
	}
}

void ASHWeapon::EndReloading()
{
	CurrentBullets = MagazineSize;
	bIsReloading = false;
}

void ASHWeapon::Fire()
{
	if (bIsReloading)
	{
		return;
	}

	AActor* MyOwner = GetOwner();
	if (!MyOwner)
	{
		return;
	}

	if (CurrentBullets <= 0)
	{
		StartReloading();

	}
	else
	{
		// Trace the world, from weapon muzzle location to crosshair location
		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);

		PlayShotEffects(MuzzleLocation);

		// Get the location to aim
		FVector EyeLocation;
		FRotator EyeRotator;
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotator); // Fills the vectors with data

		// Location for the projectile destiny
		FVector ShotDirection = EyeRotator.Vector();

		// Spread the bullets to avoid Divinity aim
		float HalfRad = FMath::DegreesToRadians(BulletsSpread);
		ShotDirection = FMath::VRandCone(ShotDirection, HalfRad, HalfRad);

		FVector TraceEnd = EyeLocation + (ShotDirection * 5000);

		// Tracer end point particle system (modified if hits)
		FVector TracerEndPoint = TraceEnd;

		// Collision for the LineTrace
		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(MyOwner); // Avoid to collide with the owner
		QueryParams.AddIgnoredActor(this); // Avoid to collide with himself
		//QueryParams.bTraceComplex = true; // More expensive but more accurate
		QueryParams.bReturnPhysicalMaterial = true; // Needed to know what material is hitting

		FHitResult Hit;
		EPhysicalSurface SurfaceType = SurfaceType_Default;
		bool BlockingHit = GetWorld()->LineTraceSingleByChannel(Hit, MuzzleLocation, TraceEnd, ECC_Visibility, QueryParams);
		DrawDebugLine(GetWorld(), MuzzleLocation, TraceEnd, FColor::Red, false, 1.0f, 0, 1.0f);

		// Damage process
		if (BlockingHit)
		{
			AActor* HitActor = Hit.GetActor();

			SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

			UGameplayStatics::ApplyPointDamage(HitActor, AmountDamage, ShotDirection, Hit, MyOwner->GetInstigatorController(), this, DamageType);

			PlayImpactEffects(SurfaceType, Hit.ImpactPoint);

			TracerEndPoint = Hit.ImpactPoint;
		}

		// Update time for the fire rate
		LastFireTime = GetWorld()->TimeSeconds;
		CurrentBullets--;
	}
}