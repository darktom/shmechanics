#include "Components/SHHealthComponent.h"

#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
USHHealthComponent::USHHealthComponent()
{
	MaxHealth = 100.0f;
}

// Called when the game starts
void USHHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	AActor* MyOwner = GetOwner();
	if (MyOwner)
	{
		MyOwner->OnTakeAnyDamage.AddDynamic(this, &USHHealthComponent::HandleTakeAnyDamage);
	}

	Health = MaxHealth;
}

void USHHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0)
	{
		return;
	}

	// Update the Health clamped with the max
	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);

	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);

	UKismetSystemLibrary::PrintString(GetWorld(), TEXT("Health changed: " / FString::SanitizeFloat(Health)));
}
