#include "SHGameMode.h"

#include "SHCharacter.h"
#include "SurvivalHorrorHUD.h"
#include "UObject/ConstructorHelpers.h"

ASHGameMode::ASHGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("'/Game/Blueprints/BP_SHCharacter'"));
	if (PlayerPawnClassFinder.Succeeded())
	{
		DefaultPawnClass = PlayerPawnClassFinder.Class;
	}

	// use our custom HUD class
	HUDClass = ASurvivalHorrorHUD::StaticClass();
}