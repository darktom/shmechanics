#include "SHInteractWeapon.h"

#include "SHCharacter.h"
#include "SHWeapon.h"

ASHInteractWeapon::ASHInteractWeapon()
	: Super()
{
	WeaponReference = ASHWeapon::StaticClass();
}

void ASHInteractWeapon::Interact(ASHCharacter* CharacterInstigator)
{
	// Deletes the current weapon
	if (CharacterInstigator->CurrentWeapon)
	{
		CharacterInstigator->CurrentWeapon->Destroy();
		CharacterInstigator->CurrentWeapon = nullptr;
	}

	if (WeaponReference)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		ASHWeapon* Weapon = GetWorld()->SpawnActor<ASHWeapon>(WeaponReference, SpawnParams);;
		CharacterInstigator->CurrentWeapon = Weapon;

		if (CharacterInstigator->CurrentWeapon)
		{
			Weapon->SetOwner(CharacterInstigator);
			Weapon->AttachToComponent(CharacterInstigator->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, CharacterInstigator->GetWeaponSocketName());
		}
	}

	Destroy();

}
