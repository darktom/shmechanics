#include "SHExplosiveBarrel.h"

#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Kismet/GameplayStatics.h"

#include "Components/SHHealthComponent.h"


// Sets default values
ASHExplosiveBarrel::ASHExplosiveBarrel()
{
	HealthComp = CreateDefaultSubobject<USHHealthComponent>(TEXT("HealthComponent"));
	HealthComp->OnHealthChanged.AddDynamic(this, &ASHExplosiveBarrel::OnHealthChanged);

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	MeshComp->SetSimulatePhysics(true);
	MeshComp->SetCollisionObjectType(ECC_PhysicsBody);

	ForceComp = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	ForceComp->SetupAttachment(MeshComp);
	ForceComp->Radius = 250;
	ForceComp->bImpulseVelChange = true;
	ForceComp->bAutoActivate = false; // Prevent component from ticking, and only use FireImpulse instead
	ForceComp->bIgnoreOwningActor = true; // Ignore self elements

	RadialDamage = 80;
	bActivated = false;
}

void ASHExplosiveBarrel::OnExploded()
{
	// Play FX and change the material
	if (MeshComp && BurnedMaterial)
	{
		MeshComp->SetMaterial(0, BurnedMaterial);
	}

	if (ExplosionParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticles, GetActorLocation());
	}
}

void ASHExplosiveBarrel::OnHealthChanged(USHHealthComponent* HealthComponent, float Health, float HealthDelta, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0 && !bActivated)
	{
		// Flag to know if exploded already
		bActivated = true;
		OnExploded();

		// Boost the barrel upwards
		FVector BoostIntensity = FVector::UpVector * 400;
		MeshComp->AddImpulse(BoostIntensity, NAME_None, true);

		// Apply the force to nearby elements
		ForceComp->FireImpulse();

		// Apply radial damage
		int MinRadialDamage = RadialDamage * 0.1f;

		UGameplayStatics::ApplyRadialDamage(GetWorld(), RadialDamage, GetActorLocation(), ForceComp->Radius, nullptr, TArray<AActor*>(), this, GetInstigatorController());
	}
}
