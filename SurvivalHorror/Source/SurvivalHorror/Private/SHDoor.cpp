#include "SHDoor.h"

#include "SHCharacter.h"
#include "Components/StaticMeshComponent.h"

ASHDoor::ASHDoor()
	:Super()
{
	PrimaryActorTick.bCanEverTick = true;

	bIsBlocked = false;
	bWantsToOpen = false;
	OpeningSpeed = 7.5f;
	TargetRotationZ = 90.0f;
}

void ASHDoor::Interact(ASHCharacter* CharacterInstigator)
{
	if (bIsBlocked)
	{
		if (CharacterInstigator)
		{
			if (CharacterInstigator->Inventory.Contains(Unlocker))
			{
				Unlock();
				CharacterInstigator->Inventory.Remove(Unlocker);
			}
			else
			{
				// Door locked and player not have the key
				OnDoorLocked();
			}
		}
	}

	Open();

}

void ASHDoor::Unlock()
{
	bIsBlocked = false;
}

void ASHDoor::Open()
{
	if (!bIsBlocked)
	{
		bWantsToOpen = true;
		TargetRotationZ = 90;
	}
}

void ASHDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!bIsBlocked)
	{
		FRotator CurrentRotator = MeshComp->GetRelativeRotation();
		if (bWantsToOpen)
		{
			// Avoid to collides with the player
			MeshComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
			float NewRotationZ = FMath::FInterpTo(CurrentRotator.Yaw, TargetRotationZ, DeltaTime, OpeningSpeed);
			CurrentRotator.Yaw = NewRotationZ;

			MeshComp->SetRelativeRotation(CurrentRotator);

			if (CurrentRotator.Yaw >= 89.9)
			{
				TargetRotationZ = 0;
			}
			else
			{
				if (!bIsBlocked && CurrentRotator.Yaw <= 0.1)
				{
					// Reset the flag, the door is currently closed
					bWantsToOpen = false;
					MeshComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
				}
			}
		}
	}
}
