// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SHCharacter.generated.h"

class USkeletalMeshComponent;
class UCameraComponent;
class UInputComponent;
class USHHealthComponent;
class ASHWeapon;
class ASHInteractable;
class UPickable;

UCLASS(config = Game)
class SURVIVALHORROR_API ASHCharacter : public ACharacter
{
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDieDelegateSignature);

	GENERATED_BODY()

public:

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Delegates")
		FOnDieDelegateSignature OnDieDelegate;

	// Base turn rate, in deg/sec. Other scaling may affect final turn rate
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	// Base look up/down rate, in deg/sec. Other scaling may affect final rate
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		ASHWeapon* CurrentWeapon; // Constructed in BeginPlay Function

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		TArray<UPickable*> Inventory;

	ASHCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddInteractable(ASHInteractable* Interactable);
	void DeleteInteractable(ASHInteractable* Interactable);

	UFUNCTION(BlueprintCallable, Category = "Character")
		void Interact();

	// Weapon actions
	UFUNCTION(BlueprintCallable, Category = "Character")
		void StartFireWeapon();

	UFUNCTION(BlueprintCallable, Category = "Character")
		void StopFireWeapon();

	FORCEINLINE UCameraComponent* GetCameraComp() const { return CameraComp; }
	FORCEINLINE FName GetWeaponSocketName() const { return WeaponSocketName; }
protected:

	// First person camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USHHealthComponent* HealthComp;

	UPROPERTY(EditDefaultsOnly, Category = "Player")
		float ZoomedFOV;

	UPROPERTY(EditDefaultsOnly, Category = "Player", meta = (ClampMin = 0.1, ClampMax = 80))
		float ZoomInterpSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		TArray<ASHInteractable*> InteractableActors;

	// Default Field Of View set during begin play
	float DefaultFOV;

	// Flag to make zoom
	bool bWantsToZoom;

	// Default skeletal mesh socket name to attach the weapon
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		FName WeaponSocketName;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
		bool bDied;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
		bool bIsSprinting;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
		float WalkingSpeedMax;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
		float SprintingSpeedMax;

	virtual void BeginPlay();
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

	void KeyPressed(FKey key);

	// Weapon actions
	void ReloadWeapon();

	// Crouch actions
	void BeginCrouch();
	void EndCrouch();

	// Zoom actions
	void BeginZoom();
	void EndZoom();

	void BeginSprint();
	void EndSprint();

	UFUNCTION(BlueprintImplementableEvent, Category = "SHCharacter")
		void PlaySprintingEffects();

	UFUNCTION(BlueprintImplementableEvent, Category = "SHCharacter")
		void StopSprintingEffects();

	UFUNCTION(BlueprintImplementableEvent, Category = "SHCharacter")
		void PlayOnDieEffects();

	void MoveForward(float Value);
	void MoveRight(float Value);

	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

	UFUNCTION()
		void OnHealthChanged(USHHealthComponent* HealthComponent, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
};
