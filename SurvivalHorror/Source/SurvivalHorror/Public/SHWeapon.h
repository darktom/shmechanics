#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SHWeapon.generated.h"

class USoundCue;

UCLASS()
class SURVIVALHORROR_API ASHWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASHWeapon();

	virtual void StartFire();
	virtual void StopFire();

	virtual void StartReloading();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
		virtual void EndReloading();

	UFUNCTION(BlueprintImplementableEvent, Category = "Weapon")
		void PlayReloadingEffects();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		float AmountDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		int MagazineSize;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
		int CurrentBullets;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
		bool bIsReloading;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USkeletalMeshComponent* MeshComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		TSubclassOf<UDamageType> DamageType;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		FName MuzzleSocketName;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		FName TracerTargetName;

	FTimerHandle TimerHandle_TimeBetweenShots;
	float LastFireTime;

	// RPM - Bullets per minute fired
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		float RateOfFire;

	// Bullets spread in degrees
	UPROPERTY(EditDefaultsOnly, Category = "Weapon", meta = (ClampMin = 0.0f))
		float BulletsSpread;

	// Derived from RateOfFire
	float TimeBetweenShots;

	virtual void BeginPlay() override;

	/* Event to spawn particles at muzzle and play camera shake */
	UFUNCTION(BlueprintImplementableEvent, Category = "SHWeapon")
		void PlayShotEffects(FVector MuzzleLocation);

	UFUNCTION(BlueprintImplementableEvent, Category = "SHWeapon")
		void PlayImpactEffects(EPhysicalSurface SurfaceType, FVector ImpactPoint);

	virtual void Fire();

};