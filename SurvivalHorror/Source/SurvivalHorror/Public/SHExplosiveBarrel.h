// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SHExplosiveBarrel.generated.h"

class UCoopHealthComponent;
class UStaticMeshComponent;
class URadialForceComponent;


UCLASS()
class SURVIVALHORROR_API ASHExplosiveBarrel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASHExplosiveBarrel();

protected:

	// Amount of damage to deal around
	UPROPERTY(VisibleAnywhere, Category = "Components")
		int RadialDamage;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		USHHealthComponent* HealthComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		URadialForceComponent* ForceComp;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
		UMaterial* BurnedMaterial;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
		UParticleSystem* ExplosionParticles;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool bActivated;

	UFUNCTION()
		void OnExploded();

	UFUNCTION()
		void OnHealthChanged(USHHealthComponent* HealthComponent, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
};
