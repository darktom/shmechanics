// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SHInteractable.h"
#include "SHDoor.generated.h"

class ASHCharacter;

UCLASS()
class SURVIVALHORROR_API ASHDoor : public ASHInteractable
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsBlocked;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bWantsToOpen;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UPickable* Unlocker;

	UPROPERTY(EditDefaultsOnly, Category = "Door", meta = (ClampMin = 0.1, ClampMax = 80))
		float OpeningSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float TargetRotationZ;

	ASHDoor();

	virtual void Interact(ASHCharacter* CharacterInstigator) override;

	void Unlock();
	void Open();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "SHDoor")
		void OnDoorLocked();
};
