// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SHInteractable.h"
#include "SHInteractWeapon.generated.h"

class ASHWeapon;

/**
 *
 */
UCLASS()
class SURVIVALHORROR_API ASHInteractWeapon : public ASHInteractable
{
	GENERATED_BODY()

public:

	ASHInteractWeapon();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "InteractableWeapon")
		TSubclassOf<ASHWeapon> WeaponReference;

	virtual void Interact(ASHCharacter* CharacterInstigator) override;

};
